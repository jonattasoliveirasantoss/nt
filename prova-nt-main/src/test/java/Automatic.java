import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Automatic {

    @Test
    public void pesquisarGoogle() {
        //**1. Acessar a página:
        //https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap
        //2. Mudar o valor da combo Select version para “Bootstrap V4 Theme”
        //3. Clicar no botão Add Customer

        System.setProperty("webdriver.chrome.driver", "src\\drive\\chromedriver.exe");
        WebDriver navegador = new ChromeDriver();
        navegador.get("https://www.grocerycrud.com/v1.x/demo/my_boss_is_in_a_hurry/bootstrap");
        navegador.findElement(By.xpath("//*[@id=\"switch-version-select\"]")).click();
        navegador.findElement(By.xpath("//*[@id=\"switch-version-select\"]/option[4]")).click();
        navegador.findElement(By.xpath("//*[@id=\"gcrud-search-form\"]/div[1]/div[1]/a")).click();
        //4. Preencher os campos do formulário com as seguintes informações:
        //
        //Name: Teste Sicredi
        //Last name: Teste
        //ContactFirstName: seu nome
        //Phone: 51 9999-9999
        //
        //Classificação da informação: Uso Interno
        //
        //AddressLine1: Av Assis Brasil, 3970
        //AddressLine2: Torre D
        //City: Porto Alegre
        //State: RS
        //PostalCode: 91000-000
        //Country: Brasil
        //from Employeer: Fixter
        //CreditLimit: 200
        navegador.findElement(By.xpath("//*[@id=\"field-customerName\"]")).sendKeys("Teste Sicredi");
        navegador.findElement(By.xpath("//*[@id=\"field-contactLastName\"]")).sendKeys("Teste");
        navegador.findElement(By.xpath("//*[@id=\"field-contactFirstName\"]")).sendKeys("Jonattas Oliveira Santos(*seu nome)");
        navegador.findElement(By.xpath("//*[@id=\"field-phone\"]")).sendKeys("51 9999-9999");
        navegador.findElement(By.xpath("//*[@id=\"field-addressLine1\"]")).sendKeys("Av Assis Brasil, 3970");
        navegador.findElement(By.xpath("//*[@id=\"field-addressLine2\"]")).sendKeys("Torre D");
        navegador.findElement(By.xpath("//*[@id=\"field-city\"]")).sendKeys("Porto Alegre");
        navegador.findElement(By.xpath("//*[@id=\"field-state\"]")).sendKeys("RS");
        navegador.findElement(By.xpath("//*[@id=\"field-postalCode\"]")).sendKeys("91000-000");
        navegador.findElement(By.xpath("//*[@id=\"field-country\"]")).sendKeys("Brasil");
        navegador.findElement(By.xpath("//*[@id=\"field-salesRepEmployeeNumber\"]")).sendKeys("Fixter");
        navegador.findElement(By.xpath("//*[@id=\"field-creditLimit\"]")).sendKeys("200");
        //5. Clicar no botão Save
        navegador.findElement(By.xpath("//*[@id=\"form-button-save\"]")).click();
        //6. Clicar no botão Delete da popup, aparecerá uma mensagem dentro de um box verde
        //na parte superior direito da tela. Adicione uma asserção na mensagem “Your data has
        //been successfully deleted from the database.”
        //7. Fechar o browser

        //navegador.findElement(By.xpath("//*[@id=\"report-success\"]/p")).getText();
        //String validacao = this.toString(By.ByXPath("//*[@id=\"report-success\"]/p"));
        //return validacao.contains("Your data has been successfully stored into the database. ");


        navegador.quit();

    }
}
