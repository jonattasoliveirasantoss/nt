package br.com.sicredi.simulacao.api.restricao;

import com.jayway.restassured.RestAssured;
import io.restassured.specification.Argument;
import org.hamcrest.Matcher;
import org.junit.Test;

import java.util.List;

import static com.sun.javafx.fxml.expression.Expression.equalTo;
import static io.restassured.RestAssured.given;

public class RestricaoAPITeste {
    @Test
    public void testRestAssured() {
        RestAssured.baseURI = "http://localhost:8081";

        given()
                .pathParam("cpf", "97093236014")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 24094592008  tem problema"));

        given()
                .pathParam("cpf", "60094146012")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 24094592008  tem problema"));

        given()
                .pathParam("cpf", "84809766080")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 24094592008  tem problema"));

        given()
                .pathParam("cpf", "62648716050")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 62648716050  tem problema"));

        given()
                .pathParam("cpf", "26276298085")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 26276298085  tem problema"));

        given()
                .pathParam("cpf", "01317496094")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 01317496094  tem problema"));

        given()
                .pathParam("cpf", "55856777050")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 55856777050  tem problema"));

        given()
                .pathParam("cpf", "19626829001")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 19626829001  tem problema"));

        given()
                .pathParam("cpf", "24094592008")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 24094592008  tem problema"));

        given()
                .pathParam("cpf", "58063164083")
                .when()
                .get("/api/v1/restricoes/{cpf}")
                .then()
                .statusCode(200)
                .body("mensagem", equalTo("o cpf 58063164083  tem problema"));







    }

    private Matcher<?> equalTo(String s) {
        return null;
    }

